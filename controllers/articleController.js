const models  = require('../models');
const passwordTools = require("../utils/passwordTools");
const tools = require("../utils/commonTools");
const errorMsgObj = require('../configs/errorMessageObjects');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const validator = require('express-validator/check');
const { sanitizeBody } = require('express-validator');

exports.validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                validator.body('subject').isLength({min: 5, max: 100 }),
                sanitizeBody('content').escape(),
                sanitizeBody('subject').escape()
            ]
        }
        case 'update': {
            return [
                validator.body('subject').isAlphanumeric().isLength({min: 5,max: 100 }),
                sanitizeBody('content').escape(),
                sanitizeBody('subject').escape(),
                validator.param('articleId').isNumeric()
            ]
        }
        case 'remove': {
            return [
                validator.param('articleId').isNumeric()
            ]
        }
        case 'findOne': {
            return [
                validator.param('articleId').isNumeric()
            ]
        }
        case 'findAll': {
            return [
                validator.param('pageNumber').optional().isNumeric()
            ]
        }
    }
};



exports.remove=function(req, res) {

    var errors = validator.validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {

        res.status(422).json(tools.getReturnJSON(false, errors, errorMsgObj.form_invalid));
        return;
    }

    let {articleId} =req.params;

    let userId=req.accountId || 5;


    models.Article.destroy({
        where: {
            userId: {
                [Op.eq]: userId
            },
            id: {
                [Op.eq]: articleId
            }
        }
    }).then(affectedRowCount => {

        if(affectedRowCount){
            res.json(tools.getReturnJSON(true, {"deleteRows":affectedRowCount}));
        }else{
            res.json(tools.getReturnJSON(false, [], errorMsgObj.delete_article_failed));
        }

    });

};


exports.update = function(req, res) {

    var errors = validator.validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {

        res.status(422).json(tools.getReturnJSON(false, errors, errorMsgObj.form_invalid));
        return;
    }

    let {subject, content} =req.body;

    models.sequelize.transaction(t => {
        // chain all your queries here. make sure you return them.
        return models.Article.update({ subject: subject, content: content}, {
                where: {
                    userId: {
                        [Op.eq]: req.accountId
                    },
                    id: {
                        [Op.eq]: req.params.articleId
                    }
                }
            },
            {transaction: t})

    }).then(affectedRowCount => {

        if(affectedRowCount){
            res.json(tools.getReturnJSON(true, {"updateRows":affectedRowCount[0]}));
        }else{
            res.json(tools.getReturnJSON(false, "", errorMsgObj.update_article_failed));
        }

    }).catch(err => {

        console.log(err);
        res.json(tools.getReturnJSON(false, "", errorMsgObj.db_error));

    });


};


exports.create = function(req, res) {

    let errors = validator.validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {

        res.status(422).json(tools.getReturnJSON(false, errors, errorMsgObj.form_invalid));
        return;
    }

    let {subject, content} =req.body;

    let userId=req.accountId;

    models.sequelize.transaction(t => {
        // chain all your queries here. make sure you return them.
        return models.Article.create({ subject: subject, content: content, userId:userId  }
            ,{transaction: t})

    }).then(article => {

        if(article){
            res.json(tools.getReturnJSON(true, article.id));
        }else{
            res.json(tools.getReturnJSON(false, [], errorMsgObj.insert_article_failed));
        }

    }).catch(err => {

        console.log(err);
        res.json(tools.getReturnJSON(false, [], errorMsgObj.db_error, err));

    });


};

exports.findOne = function(req, res){

    let errors = validator.validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {

        res.status(422).json(tools.getReturnJSON(false, errors, errorMsgObj.form_invalid));
        return;
    }

    models.Article.findOne({
        attributes: ['subject', 'content','User.name'],
        include: [{
            nested: false,
            model: models.User,
            attributes: []
            //where: { id: 1 }
        }],
        raw: true,
        where: {
            id: {
                [Op.eq]: req.params.articleId
            }
        }
    }).then(articles => {

        if(articles){
            res.json(tools.getReturnJSON(true, articles));
        }else{
            res.json(tools.getReturnJSON(false, [], errorMsgObj.get_article_failed));
        }

    })
};

exports.findAll = function(req, res){

    let errors = validator.validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {

        res.status(422).json(tools.getReturnJSON(false, errors, errorMsgObj.form_invalid));
        return;

    }


    const limit=10;
    let offset=(req.params.pageNumber && req.params.pageNumber >=1)?req.params.pageNumber:1;
        offset=(offset-1)*limit || 0;


    models.Article.findAndCountAll({
        attributes: ['id','subject', 'content','User.name','createdAt'],
        include: [{
            nested: false,
            model: models.User,
            attributes: []
            //where: { id: 1 }
        }],
        raw: true,
        limit: limit,
        offset: offset,
        order:[['createdAt', 'DESC']]
    }).then(articles => {

        if(articles){
            res.json(tools.getReturnJSON(true, articles));
        }else{
            res.json(tools.getReturnJSON(false, [], errorMsgObj.get_article_list_failed));
        }

    })

};