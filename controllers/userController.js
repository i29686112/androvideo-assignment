const models  = require('../models');
const passwordTools = require("../utils/passwordTools");
const tools = require("../utils/commonTools");
const errorMsgObj = require('../configs/errorMessageObjects');
const Sequelize = require('sequelize');

const validator = require('express-validator');
const { sanitizeBody } = require('express-validator');


exports.validate = (method) => {
    switch (method) {
        case 'register': {
            return [
                validator.body('name').isLength({ max: 50 }),
                validator.body('account')
                    .isAlphanumeric()
                    .isLength({ max: 50 })
                    .custom(value => {
                        return models.User.findOne({ where: {account: value} }).then(user=>{
                            if (user) {
                                throw new Error('this account is already in use');
                            }
                        })
                    }),
                validator.body('password').isLength({ min:1}),
                sanitizeBody('name').escape()
            ]
        }
        case 'login': {
            return [
                validator.body('account').isAlphanumeric(),
                validator.body('password').isLength({ min:1})
            ]
        }
    }
};


exports.register = function(req, res){

    var errors = validator.validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {

        console.log(errors);
        res.status(422).json(tools.getReturnJSON(false, errors, errorMsgObj.form_invalid));
        return;
    }


    const {name,account,password}=req.body;



    //valid
    passwordTools.cryptPassword(password,function(err, hash){

        return models.sequelize.transaction(t => {
            // chain all your queries here. make sure you return them.
            return models.User.create(
                { name: name, account: account, password: hash },
                {transaction: t})

        }).then(user => {

            if(user){

                var token = passwordTools.tokenGenerate(user.id,user.account,Math.floor(new Date() / 1000));

                res.json(tools.getReturnJSON(true, token));
            }else{
                res.json(tools.getReturnJSON(false, [], errorMsgObj.insert_user_failed));
            }

        }).catch(err => {

            res.json(tools.getReturnJSON(false, [], errorMsgObj.db_error,err));

        });

    });





};

exports.login = function(req, res){

    var errors = validator.validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {

        res.status(422).json(tools.getReturnJSON(false, errors, errorMsgObj.form_invalid));
        return;
    }

    const {account,password}=req.body;

    models.User.findOne({ where: {account: account} }).then(user => {

        if(user===null){
            res.json(tools.getReturnJSON(false, [],errorMsgObj.login_failed));
            return false;
        }


        passwordTools.comparePassword(password, user.password, function(err,isSame){

            if(isSame){
                var token = passwordTools.tokenGenerate(user.id,user.account,Math.floor(new Date() / 1000));
                res.json(tools.getReturnJSON(true, token));
            }else{
                res.json(tools.getReturnJSON(false, [], errorMsgObj.login_failed));

            }

        })


    })

};