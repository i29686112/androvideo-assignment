const express = require('express');
const router = express.Router();

const user = require("../controllers/userController");


router.post('/register',[user.validate("register")], user.register);

router.post('/login',[user.validate("login")], user.login);


module.exports = router;
