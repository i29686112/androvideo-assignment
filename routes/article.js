const express = require('express');
const router = express.Router();
const article = require("../controllers/articleController");
const authMiddleware = require("../middlewares/authMiddleware");


/* GET all articles. */
router.get('/getAll/:pageNumber?*',[article.validate("findAll")], article.findAll);

router.get('/', article.findAll);


/* get specific article. */
router.get('/:articleId',[article.validate("findOne")], article.findOne);


//create
router.post('/',[authMiddleware,article.validate("create")], article.create);

//update
router.put('/:articleId',[authMiddleware,article.validate("update")], article.update);

router.delete('/:articleId',[authMiddleware,article.validate("remove")], article.remove);

module.exports = router;
