'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id:{
      primaryKey:true,
      type:DataTypes.INTEGER,
      autoIncrement:true,
      allowNull:false
    },
    name:{
      type:DataTypes.STRING,
      allowNull:false,
      require:true
    },
    account: {
      type:DataTypes.STRING,
      allowNull:false,
      require:true,
      unique:true
    },
    password:{
      type:DataTypes.STRING,
      allowNull:false,
      require:true
    }
  }, {
    timestamps: true
  });
  User.associate = function(models) {
    // associations can be defined here

    models.User.hasMany(models.Article,{

      onDelete: "CASCADE",
      foreignKey: {
          allowNull: false,
            name: 'userId'
      }
    , sourceKey: 'id'
    });


  };
  return User;
};