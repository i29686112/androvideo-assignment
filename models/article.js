'use strict';
module.exports = (sequelize, DataTypes) => {
  const Article = sequelize.define('Article', {
    id:{
      primaryKey:true,
      type:DataTypes.INTEGER,
      autoIncrement:true,
      allowNull:false
    },
    subject: {
      type:DataTypes.STRING,
      allowNull:false,
      require:true
    },
    content: {
      type:DataTypes.TEXT,
      allowNull:false,
      require:true
    },
    userId: DataTypes.BIGINT(11)
  }, {timestamps: true});
  Article.associate = function(models) {
    // associations can be defined here

    models.Article.belongsTo(models.User, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false,
        name: 'userId'
      }
      , targetKey: 'id'
    });


  };
  return Article;
};