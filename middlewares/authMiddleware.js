var webConfig = require('../configs/webConfig');
var jwt = require('jsonwebtoken');
var tools = require("../utils/commonTools");
var errorMsgObj = require('../configs/errorMessageObjects');

module.exports = function(req,res,next){


    if (!req.headers.authorization) {
        return res.status(401).json(tools.getReturnJSON(false, [], errorMsgObj.need_token));

    }

    if (req.headers.authorization && req.headers.authorization.split(' ')[0] == 'Bearer') {
        jwt.verify(req.headers.authorization.split(' ')[1], webConfig.secret, function (err, decoded) {
            if (err) {

                switch (err.name) {
                    case 'TokenExpiredError':
                        return res.status(401).json(tools.getReturnJSON(false, [], errorMsgObj.token_expired));

                        break;

                    case 'JsonWebTokenError':
                        return res.status(401).json(tools.getReturnJSON(false, [], errorMsgObj.token_invalid));
                        break;
                }

            } else {

                //user account here
                req.accountName = decoded.accountName;
                req.accountId = decoded.accountId;


                next();
            }
        });
    }




};