
module.exports = {
    //DB CODE

    "db_error":[-901,"We're encounter db error and transaction got rollback, please check log"],



    //user
    "insert_user_failed":[-100,"Insert user to db failed."],

    //article
    "get_article_failed":[-100,"Get the article failed."],
    "insert_article_failed":[-101,"Insert article to db failed."],
    "update_article_failed":[-102,"Update an article failed."],
    "delete_article_failed":[-103,"Delete an article failed."],
    "get_article_list_failed":[-104,"Get the article list failed."],



    //for login
    "login_failed":[-100,"Login failed, please check your account & password is correct."],
    "need_token":[-101,"Please login before use the function."],
    "token_expired":[-102,"Your token is expired, please login again to generate a new token."],
    "token_invalid":[-103,"Your token is not valid, please login again to generate a new token."],

    //form

    "form_invalid":[-100,"Your submission have some errors."]




};