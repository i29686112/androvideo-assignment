# Please check the wiki page for more detail about [installation](https://gitlab.com/i29686112/androvideo-assignment/wikis/Installation) and [API document](https://gitlab.com/i29686112/androvideo-assignment/wikis/API-Document)


# RESTful API server

## Features
- Build a RESTful API with express in JavaScript.
- Define two data models, UserModel and ArticleModel.
- UserModel should include `name`, account and password required fields.
- ArticleModel should include subject and content required fields.
- Users can register and login.
- The user can create articles and get, update, delete that's articles.
- Anonymous users cat list articles, get the article details. (The article information includes the author.)
- Upload the source code to GitHub, Bitbucket or other services. And make sure we can access the project.

## Notes
- We are not need web UI. Please just build a RESTful API server.
- You can store data at the memory, sqlite, files or database.
- You can use any vendor packages.