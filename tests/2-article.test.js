

//login
before((done) => {
    api.post('/user/login') //
        .set('Accept', 'application/json')
        .send({
            "account":account,
            "password":password
        })
        .expect(200)
        .end((err, res) => {

            APIToken = res.body.result; // get JWT
            done();

        });
});


describe('Article test cases', () => {

    let newArticleId;
    let APIToken;

    before(function(done) {

        api.post('/user/login') //
            .set('Accept', 'application/json')
            .send({
                "account":account,
                "password":password
            })
            .expect(200)
            .end((err, res) => {
                APIToken = res.body.result; // get JWT
                done();

            });
    });



    it('Article create', (done) => {
        
        api.post('/article')
            .set('Authorization', "Bearer "+APIToken)
            .set('Accept', 'application/json')
            .send({
                "subject":"title1bbbba",
                "content":"abcesbb<<>>bbba"
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    done(err);
                }


                expect(res.body.result).to.be.a('number');
                newArticleId=res.body.result;
                done();
            });
    });


    it('Article show', (done) => {

        api.get('/article/'+newArticleId)
            .set('Authorization', "Bearer "+APIToken)
            .expect(200)
            .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.result.subject).to.be.a('string');
                expect(res.body.result.content).to.be.a('string');
                expect(res.body.result.name).to.be.a('string');

                done();
            });
    });

    it('Article update', (done) => {

        api.put('/article/'+newArticleId)
            .set('Authorization', "Bearer "+APIToken)
            .set('Accept', 'application/json')
            .send({
                "subject":"title1bbbba2",
                "content":"abcesbb<<>>bbba2"
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.result.updateRows).to.equal(1);
                done();
            });
    });

    //it('Article remove', (done) => {
    //
    //    api.delete('/article/'+newArticleId)
    //        .set('Authorization', "Bearer "+APIToken)
    //        .expect(200)
    //        .end((err, res) => {
    //            if (err) {
    //                done(err);
    //            }
    //
    //            expect(res.body.result.deleteRows).to.equal(1);
    //            done();
    //        });
    //});

});