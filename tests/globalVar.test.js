//functions
global.faker = require('faker');
global.supertest = require('supertest');
global.api = supertest('http://localhost:3000'); //test path
global.expect= require('chai').expect;


//var
global.name=faker.name.firstName(1)+ " "+faker.name.lastName(1);
global.account=faker.random.alphaNumeric(10);
global.password=faker.random.alphaNumeric(10);