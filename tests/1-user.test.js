describe('User test cases', () => {
    it('User register', (done) => {
        let testObj={
            "name":name,
            "account":account,
            "password":password
        };

        api.post('/user/register')
            .set('Accept', 'application/json')
            .send(testObj)
            .expect(200)
            .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.result).to.be.a('string');
                done();
            });
    });

    it('User login', (done) => {
        api.post('/user/login')
            .set('Accept', 'application/json')
            .send({
                "account":account,
                "password":password
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.result).to.be.a('string');
                done();
            });
    });

});


