const _ = require('underscore');

/**
 * Generate a json object for http response
 * @param success true/false
 * @param result
 * @param errorMsgObj
 * @param other_error_message
 * @returns {{}}
 */
exports.getReturnJSON = function (success, result, errorMsgObj, other_error_message) {

    var jsonObject = {};

    result=  result || [];

    if(_.has(result,"errors")){
        result = result["errors"].map(error => error.param+":"+error.msg)
    }
    jsonObject.result = result;

    if (success) {

        jsonObject.success = true;

    } else {

        if (errorMsgObj != null) {

            jsonObject.success = false;
            jsonObject.errorCode = errorMsgObj[0];
            jsonObject.errorMsg =  errorMsgObj[1];


            //if it have self-defined error message, add it into errorMsg field
            if (other_error_message) {
                if (_.isArray(other_error_message)) {
                    for (var i = 0; i < other_error_message.length; i++) {
                        jsonObject.errorMsg = jsonObject.errorMsg + "\n" + other_error_message[i];
                    }
                } else if (_.isObject(other_error_message)) {
                    Object.keys(other_error_message).forEach(function (key) {
                        var val = other_error_message[key];
                        jsonObject.errorMsg = jsonObject.errorMsg + "\n" + val;
                    });
                } else {
                    jsonObject.errorMsg = jsonObject.errorMsg + "\n" + other_error_message;
                }
            }

        } else {
            //if the error code isn't been defined, return system error message

            jsonObject.success = false;
            jsonObject.result = [];
            jsonObject.errorCode = -9999;
            jsonObject.errorMsg = "System error"

        }

    }

    return jsonObject;

};