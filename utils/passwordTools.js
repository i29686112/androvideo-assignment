var bcrypt = require('bcrypt');
var crypto = require('crypto');
var base64url = require('base64url');
var webConfig = require('../configs/webConfig');
var jwt = require('jsonwebtoken');

exports.cryptPassword = function(password, callback) {
    bcrypt.genSalt(10, function(err, salt) {
        if (err)
            return callback(err);

        bcrypt.hash(password, salt, function(err, hash) {
            return callback(err, hash);
        });
    });
};

exports.comparePassword = function(plainPass, hashword, callback) {

    if(hashword===""){
        callback(null, false);
    }

    bcrypt.compare(plainPass, hashword, function(err, isPasswordMatch) {
        return err == null ?
            callback(null, isPasswordMatch) :
            callback(err);
    });
};


exports.tokenGenerate = function(userId,userAccount,createTimestamp) {

    var secret = webConfig.secret;

    var header = {
        alg: 'HS256',
        typ: 'JWT'
    };
    header = base64url(JSON.stringify(header));

    var payload = {
        sub: 'User Login',
        accountName: userAccount,
        accountId: userId,
        iat: createTimestamp,
        exp: createTimestamp+webConfig.sessionLifetime
    };
    payload = base64url(JSON.stringify(payload));

    var signature = crypto.createHmac('SHA256', secret)
        .update(header + '.' + payload)
        .digest('base64');
    signature = base64url.fromBase64(signature);


    return header + '.' + payload + '.' + signature;


};